
export const currencyFormat = (num, deci) => {
    if (!deci || deci===0)
        return '$' + (num+'').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')    
    else
        return '$' + num.toFixed(deci).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}


// export default currencyFormat;