import React, { Component } from 'react';

import Meta from '../components/Meta';

import CarouselBasic from '../components/CarouselBasic'
import SectionIntro from '../components/SectionIntro'
import CardCategory from '../components/CardCategory'
import CardRecommand from '../components/CardRecommand'
// Service 
import {getHelmet, getCarousel, getIntroduction, getProduct, getCategory} from '../services';

class Index extends Component {

    constructor() {
        super();
        this.state = {
            meta: null,
            carousel: [],
            category: [],
            intro: null,
            recommand: []
        };

        this.getHelmetData().then (() => {
            this.getCarouselData()
        }).then( () => {
            this.getIntroData()
        }).then( () => {
            this.getCateData()
        }).then( () => {
            this.getRecommandProduct()
        });
    }
    
    async getHelmetData () {
        this.setState({meta: await getHelmet()});
    }

    async getCarouselData () {
        this.setState({carousel: await getCarousel()});
    }

    async getIntroData () {
        this.setState({intro: await getIntroduction()});
    }

    async getCateData () {
        this.setState({category: await getCategory()});
    }

    async getRecommandProduct () {
        this.setState({recommand: await getProduct.byRecommand()});
    }

    render() { 
        return (
            <main className="index">
                <Meta {...this.state.meta} />
                {/* 輪播圖區塊 */}
                <CarouselBasic carousel={this.state.carousel} parallax={true} />
                {/* 圖文區塊*/}
                {
                    (null !== this.state.intro) ? (
                        <SectionIntro {...this.state.intro} transition={'fade'} />
                    ) : ''
                }
                {/* 分類牌卡區塊 */}
                {
                    (this.state.category.length) ? (
                        <CardCategory parallax={true} title={"熱門分類"} data={this.state.category} transition={'grow'} />
                    ) : ''
                }
                {/* 推薦商品牌卡區塊 */}
                <CardRecommand title={"推薦商品"} data={this.state.recommand} />
            </main>
        );
    }
}
 
export default Index;