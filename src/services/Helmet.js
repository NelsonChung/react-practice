import {Helmet as HelmetMock} from '../mock'

const getHelmet = async () => {
    return await HelmetMock;
}


export default getHelmet;