import {Category as CategoryMock} from '../mock';

const getCategory = async () => {
    return await CategoryMock;
}

export default getCategory;
