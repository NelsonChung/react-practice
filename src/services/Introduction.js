import {Introduction as IntroductionMock} from '../mock'

const getIntroduction = async () => {
    return await IntroductionMock;
}


export default getIntroduction;