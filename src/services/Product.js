import {Product as ProductMock} from '../mock';

const getProduct = {
    all: async () => {
        return await ProductMock;
    },
    byCategory: async (category) => {
        
        return await ProductMock.filter(prod => prod.category.indexOf(category) > -1);
    },
    byRecommand: async () => {
        return await ProductMock.filter(prod => prod.recommand===true);
    }
}

export default getProduct;
