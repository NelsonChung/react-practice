import {Carousel as CarouselMock} from '../mock';

const getCarousel = async () => {
    return await CarouselMock;
}

export default getCarousel;
