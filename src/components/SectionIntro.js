import React, { Component } from 'react';
import { Box, Typography, Grid } from '@material-ui/core';
import { Fade } from '@material-ui/core';
import VisibilitySensor from 'react-visibility-sensor';

class SectionIntro extends Component {
    constructor (props) {
        super(props);
        this.state = {
            isShow: false
        }

        if (!this.props.transition) this.setState({isShow: true});
    }

    visibilityChange (isVisible) {
        if(isVisible === true) {
            this.setState({isShow: true});
        }
    }
    
    render () {
        return (
            <VisibilitySensor onChange={this.visibilityChange.bind(this)}>
                <Box component="section" className="intro">
                    <Grid container spacing={2}>
                        <Grid item md={3} sm={12}>
                            {
                                (!this.props.transition) 
                                ? (
                                    <Box>
                                        <Typography variant="h2">{this.props.title}</Typography>
                                        <Typography variant="body1">{this.props.description}</Typography>
                                    </Box>
                                ) 
                                : (
                                    <Fade in={this.state.isShow} className={`float-caption left-center is-transition with-slide-from-left ${this.state.isShow ? 'is-in': ''}`}>
                                        <Box>
                                            <Typography variant="h2">{this.props.title}</Typography>
                                            <Typography variant="body1">{this.props.description}</Typography>
                                        </Box>
                                    </Fade>
                                )
                            }
                        </Grid>
                        <Grid item md={9} sm={12}>
                            {
                                (!this.props.transition) 
                                ? (
                                    <Box>
                                        <img src={this.props.image} alt="demo-img" />
                                    </Box>
                                ) 
                                : (
                                    <Fade in={this.state.isShow} className={`image-box with-slide-from-right is-transition ${this.state.isShow ? 'is-in': ''}`}>
                                        <Box>
                                            <img src={this.props.image} alt="demo-img" />
                                        </Box>
                                    </Fade>
                                )
                            }
                        </Grid>
                    </Grid>
                </Box>
            </VisibilitySensor>
        )
    }
}

export default SectionIntro;