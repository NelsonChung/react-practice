import React, { Component } from 'react';
import Helmet from "react-helmet";

class Meta extends Component {
    constructor(props) {
        super(props);
        console.log('Meta', props)
    }

    render() {
        return (
            <Helmet>
                <title>{this.props.title}</title>
                {
                    (this.props.keywords) 
                    ? (<meta name="keywords" content={this.props.keywords} />)
                    : ''
                }
                {
                    (this.props.description) 
                    ? (<meta name="description" content={this.props.description} />)
                    : ''
                }
            </Helmet>
        );
    }
}


export default Meta;