import React, { Component } from 'react'
import {Box} from '@material-ui/core';
import {Parallax} from 'react-scroll-parallax';

class ParallaxBasic extends Component {
    constructor(props) {
        super(props);
    }

    render () {
        return (
            <Box className={`parallax-container is-${this.props.color}`}>
                <Parallax y={['20px', '-550px']}>
                    <Box component="div" className="deco-trangle left-top"/>
                </Parallax>
                <Parallax y={['50px', '-480px']}>
                    <Box component="div" className="deco-trangle right-middle"/>
                </Parallax>
            </Box>
        )
    }
}

export default ParallaxBasic;