import React from 'react';
import { Container, Box, Typography } from '@material-ui/core';

import Slider from "react-slick";
import ParallaxBasic from "./Parallax";

class CarouselBasic extends React.Component {
    constructor (props) {
        super(props);

        this.CarouselSettings = {
            dots: true,
            infinite: true,
            speed: 1000,
            fade: true,
            arrows: true,
            autoplay: true,
            slidesToShow: 1,
            slidesToScroll: 1
        };

    }

    render () {
        return (
            <Box component="section" className="banner">
                {
                    this.props.parallax ? (
                        <ParallaxBasic color={'blue'} />
                    ): ''
                }
                
                <Container>
                    <Slider {...this.CarouselSettings}>
                        {
                            this.props.carousel.length ? (
                                this.props.carousel.map((item, idx) => (
                                    <div key={idx}>
                                        <img src={item.image} alt="carousel item 1" />
                                        <div className="caption">
                                            <Typography variant="h2" component="h2" gutterBottom>
                                                {item.caption.header}
                                            </Typography>
                                            <Typography variant="body1" gutterBottom>
                                                {item.caption.description}
                                            </Typography>
                                        </div>
                                    </div>
                                ))
                            ) : ''
                        }
                    </Slider>
                </Container>
            </Box>
        )

    }
}

export default CarouselBasic;