import React, { Component } from 'react';
import {Box, Container, Card, CardContent, CardActionArea, Typography, Grow} from '@material-ui/core';
import VisibilitySensor from 'react-visibility-sensor';

import ParallaxBasic from "./Parallax";

class CardCategory extends Component {
    constructor (props) {
        super(props);
        this.state = {
            isShow: false
        }

        if (!this.props.transition) this.setState({isShow: true});
    }

    visibilityChange (isVisible) {
        console.log('visibilityChange', isVisible)
        if(isVisible === true) {
            this.setState({isShow: true})
        }
    }

    render () {
        return (
            <Box component="section" className="category">
                {
                    this.props.parallax ? (
                        <ParallaxBasic color={'orange'} />
                    ): ''
                }

                <Container>
                    <Typography variant="h2" component="h2" className="center-aligned">
                        {this.props.title}
                    </Typography>
                    <VisibilitySensor partialVisibility={true} onChange={this.visibilityChange.bind(this)}>
                        <Box component="div" className="cards three-cards">
                            {
                                this.props.data.map((item, idx) => (
                                    (!this.props.transition) 
                                    ? (
                                        <Card key={`card-${idx}`} className="category paded">
                                            <CardActionArea>
                                                <CardContent>
                                                    <Typography variant="h2" component="h2">
                                                        {item.name}
                                                    </Typography>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    )
                                    : (
                                    <Grow 
                                        key={`card-${idx}`}
                                        in={this.state.isShow} 
                                        style={{ transformOrigin: '50% 50% 0' }} 
                                        {...(this.state.isShow ? { timeout: (idx+1)*500 } : {})}
                                    >
                                        <Card className="category paded">
                                            <CardActionArea>
                                                <CardContent>
                                                    <Typography variant="h2" component="h2">
                                                        {item.name}
                                                    </Typography>
                                                </CardContent>
                                            </CardActionArea>
                                        </Card>
                                    </Grow>)
                                ))
                            }
                        </Box>
                    </VisibilitySensor>
                </Container>
            </Box>
        )
    }
}

export default CardCategory;