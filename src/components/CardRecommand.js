import React, { Component } from 'react';
import {Box, Container, Card, CardMedia, CardContent, CardActionArea, CardActions, Button, Typography, Grow} from '@material-ui/core';
import {ShoppingCart, Payment} from '@material-ui/icons'
import VisibilitySensor from 'react-visibility-sensor';

import {currencyFormat} from '../format'

class CardRecommand extends Component {
    constructor (props) {
        super(props);
        this.state = {
            isShow: false
        }

        if (!this.props.transition) this.setState({isShow: true});
    }

    visibilityChange (isVisible) {
        if(isVisible === true) {
            this.setState({isShow: true})
        }
    }

    render () {
        return (
            <Box component="section">
                <Container>
                    <Typography variant="h2" component="h2" className="center-aligned">
                        {this.props.title}
                    </Typography>
                    <VisibilitySensor partialVisibility={true} onChange={this.visibilityChange.bind(this)}>
                        <Box component="div" className="cards four-cards">
                            {
                                this.props.data.map((item, idx) => (
                                    (!this.props.transition) 
                                    ? (

                                        <Card key={`card-${idx}`} className="recommand">
                                            <CardActionArea>
                                                <CardMedia
                                                image={item.image}
                                                title={item.header}
                                                />
                                                <CardContent>
                                                    <Typography variant="body2" component="small" className="meta">
                                                        {item.category.map((cate, cid) => (
                                                            <Typography key={cid} variant="body2" component="small" className="category">
                                                                {cate}
                                                            </Typography>
                                                        ))}
                                                    </Typography>
                                                    <Typography variant="h5" component="h5">
                                                        {item.header}
                                                    </Typography>
                                                    <Typography variant="body1" component="p" className="description">
                                                        {item.description}
                                                    </Typography>
                                                    <Typography variant="h5" component="h5" className="price">
                                                        {currencyFormat(item.price)}
                                                    </Typography>
                                                </CardContent>
                                            </CardActionArea>
                                            <CardActions>
                                                <Button size="small" color="primary" startIcon={<ShoppingCart/>}>
                                                    加入購物車
                                                </Button>
                                                <Button size="small" color="primary" startIcon={<Payment/>}>
                                                    直接購買
                                                </Button>
                                            </CardActions>
                                        </Card>
                                    )
                                    : (
                                    <Grow 
                                        key={`card-${idx}`}
                                        in={this.state.isShow} 
                                        style={{ transformOrigin: '50% 50% 0' }} 
                                        {...(this.state.isShow ? { timeout: (idx+1)*500 } : {})}
                                    >
                                        <Card className="recommand">
                                            <CardActionArea>
                                                <CardMedia
                                                image={item.image}
                                                title={item.header}
                                                />
                                                <CardContent>
                                                    <Typography variant="body2" component="small" className="meta">
                                                        {item.category.map((cate, cid) => (
                                                            <Typography key={cid} variant="body2" component="small" className="category">
                                                                {cate}
                                                            </Typography>
                                                        ))}
                                                    </Typography>
                                                    <Typography variant="h5" component="h5">
                                                        {item.header}
                                                    </Typography>
                                                    <Typography variant="body1" component="p" className="description">
                                                        {item.description}
                                                    </Typography>
                                                    <Typography variant="h5" component="h5" className="price">
                                                        {currencyFormat(item.price)}
                                                    </Typography>
                                                </CardContent>
                                            </CardActionArea>
                                            <CardActions>
                                                <Button size="small" color="primary" startIcon={<ShoppingCart/>}>
                                                    加入購物車
                                                </Button>
                                                <Button size="small" color="primary" startIcon={<Payment/>}>
                                                    直接購買
                                                </Button>
                                            </CardActions>
                                        </Card>
                                    </Grow>)
                                ))
                            }
                        </Box>
                    </VisibilitySensor>
                </Container>
            </Box>
        )
    }
}

export default CardRecommand;