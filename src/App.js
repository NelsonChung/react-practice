import React from 'react';
import { BrowserRouter as Router, Route, NavLink, Switch } from "react-router-dom";
import { Container } from '@material-ui/core';
import {ParallaxProvider} from 'react-scroll-parallax';

import './sass/main.sass';

import Index from './pages/Index'
// import logo from './logo.svg';
// import './App.css';

function App() {
  return (
    <ParallaxProvider>
      <Router>
          <header>
            <Container>
              <div className="menu">
                <NavLink to="/" id="brand" className="nav-item">React Practice</NavLink>
                <NavLink to="/index" className="nav-item">Index</NavLink>
              </div>
            </Container>
          </header>
            <Switch>
              <Route path="/" exact component={Index}  />
              <Route path="/index" component={Index}  />
              {/* <Route path="/form" component={Form}  />
              <Route path="/todo" component={Todo}  />
              <Route component={NotFound}  /> */}
            </Switch>
          <footer>
            <Container>
              Powered by Nelson.
            </Container>
          </footer>
      </Router>
    </ParallaxProvider>
  );
}

export default App;
