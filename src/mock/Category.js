const Category = [
    {
        name: '暢銷'
    },
    {
        name: '文學'
    },
    {
        name: '科學'
    },
    {
        name: '社會學'
    },
    {
        name: '經典'
    },
    {
        name: '歷史'
    },
];

export default Category;