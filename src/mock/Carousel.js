const Carousel = [
    {
        image: '//placeimg.com/1600/900/arch/grayscale/1',
        caption: {
            header: '告訴我他是誰',
            description: '「暴風雨還沒開始，眼前所有的東西都安穩地站立在朦朧之中，像極了黎明時的溫柔，彷彿這一刻我們應該睡著夢著，而不應該是帶來壞消息。」'
        }
    },
    {
        image: '//placeimg.com/1600/900/arch/grayscale/2',
        caption: {
            header: '憤怒年代',
            description: '「歷史的發展看起來既不理性，也不進步。理性並不統治世界；現實明擺著就是不理性。」'
        }
    },
    {
        image: '//placeimg.com/1600/900/arch/grayscale/3',
        caption: {
            header: '全球化的時代：無政府主義，與反殖民想像',
            description: '「描繪一小群傑出人士，與一個命運的年代，既是深入動人的在地，又是寬廣無垠的全球。」'
        }
    }
];

export default Carousel;