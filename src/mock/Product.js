const Product = [
    {
        category: ['暢銷', '文學'],
        image: '//placeimg.com/1600/900/arch/grayscale/1',
        header: '告訴我他是誰',
        description: '「暴風雨還沒開始，眼前所有的東西都安穩地站立在朦朧之中，像極了黎明時的溫柔，彷彿這一刻我們應該睡著夢著，而不應該是帶來壞消息。」',
        price: 699,
        recommand: false,
        action: ['cart', 'buy']
    },
    {
        category: ['暢銷', '社會學', '歷史'],
        image: '//placeimg.com/1600/900/arch/grayscale/2',
        header: '憤怒年代',
        description: '「歷史的發展看起來既不理性，也不進步。理性並不統治世界；現實明擺著就是不理性。」',
        price: 1200,
        recommand: false,
        action: ['cart', 'buy']
    },
    {
        category: ['社會學', '歷史'],
        image: '//placeimg.com/1600/900/arch/grayscale/3',
        header: '全球化的時代：無政府主義，與反殖民想像',
        description: '「描繪一小群傑出人士，與一個命運的年代，既是深入動人的在地，又是寬廣無垠的全球。」',
        price: 489,
        recommand: true,
        action: ['cart', 'buy']
    },
    {
        category: ['文學', '暢銷'],
        image: '//placeimg.com/1600/900/arch/grayscale/3',
        header: '收集夢的剪貼簿',
        description: '我在做夢，我覺得時間走得沒有盡頭。沒有以前，也沒有以後……',
        price: 289,
        recommand: true,
        action: ['cart', 'buy']
    },
    {
        category: ['劇場', '社會學'],
        image: '//placeimg.com/1600/900/arch/grayscale/3',
        header: '冒犯觀眾',
        description: '《冒犯觀眾》以直接對觀眾發言乃至辱罵的方式，泯消了戲劇扮演，泯消了台上台下的界線，也改寫了劇場史。',
        price: 300,
        recommand: true,
        action: ['cart', 'buy']
    },
    {
        category: ['文學', '經典'],
        image: '//placeimg.com/1600/900/arch/grayscale/3',
        header: '床上的愛麗思',
        description: '桑塔格的世故，使得此劇雖犀利批判社會歷史對女性的壓抑，卻並不悲情地以血淚控訴，也不曾天真地承諾烏托邦的解放。...心智與身形的拉鋸，是桑塔格對十九世紀女性知識份子的側寫，而愛麗思‧詹姆斯的身體的敗壞，就像童話故事理的愛麗思總是發現自己的身體與世界格格不入一樣，不是太小便是太大。',
        price: 700,
        recommand: true,
        action: ['cart', 'buy']
    }
]


export default Product;